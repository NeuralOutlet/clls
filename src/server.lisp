
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))
(ql:quickload :swank-client)

;; Get swank connection if it already exists
(defvar *myswank* (swank-client:slime-connect "127.0.0.1" 4005))
(if *myswank*
	(progn
		(format t "Server already running: ~a~%" *myswank*)
		(quit)))

;; shorthand of evaluating code server-side
(defun scall (code)
	(swank-client:slime-eval code *myswank*))

(swank-loader:init)
(swank:create-server :port 4005 :dont-close t)
(setq *myswank* (swank-client:slime-connect "127.0.0.1" 4005))
(scall '(load "/home/daniel/BitBucket/helper-bar/cross-console/terminal-syntax.lisp"))
