
;;; --------------------- ;;;
;;; --- builtins.lisp --- ;;;
;;; These are all the bash builtin functions used for navigation and enviroment manipulation
;;; --------------------- ;;;


;;; --- define the package --- ;;;
(defpackage :clls
	(:use :common-lisp)
	(:export :redirect
	         :run :cd :colon :exit
	         :echo
	         :cwd))


;; function to simulate the function-name attribute of defun in other macros
;; (defun test () 5) ; here 'test' is a function-name
(defmacro raw-symbol (name)
	`(handler-case
		(check-type ,name symbol)
	(unbound-variable (uv) (cell-error-name uv))
	(:no-error () ,name)))

;; defcmd (Define Command)
; Defines a lambda using the args and body,
; binds it to the function-name and
; import/exports it into the clls package
(defmacro defcmd (name args &body body)
	`(let ()
		(locally
				(declare #+sbcl (sb-ext:muffle-conditions cl:warning))
			(handler-bind
				(#+sbcl(cl:warning #'muffle-warning))
				(setf (symbol-function (raw-symbol ,name))
					#'(lambda ,args (progn ,@body)))
				(import (raw-symbol ,name) :clls)
				(export (raw-symbol ,name) :clls)))))


;;; --- Enter package --- ;;;
(in-package :clls)

;;; --- ECL/SBCL compatability --- ;;;
(defun getenv (name &optional default)
	(or
		#+sbcl (sb-unix::posix-getenv name)
		#+ecl (si:getenv name)
		default))


;;; --- Enviroment Variables --- ;;;
(defvar *clls-last-exit* (getenv "?"))
(defvar *clls-oldpwd* (getenv "PWD"))
(defvar *clls-home* (getenv "HOME"))
(defvar *clls-pwd* (getenv "PWD"))
(defun change-dir (dir)
	(setf *clls-oldpwd* *clls-pwd*)
	(setf *clls-pwd* dir)
	(or
		#+sbcl (format t "???")
		#+ecl (ext:chdir dir)))
(defun cwd () *clls-pwd*)

;;; --- Pathname functions --- ;;;

;; Splits up a path string into list of directory components
(defun path-split (string)
	(remove ""
	(loop for start = 0 then (1+ finish)
	      for finish = (position #\/ string :start start)
		collecting (subseq string start finish)
		until (null finish))
	:test #'equal))

;; append string list with '/' between them
(defun path-concat (plist &optional (connector "/"))
	(let ((output ""))
		(loop for dir in plist
			do (setf output (concatenate 'string output connector dir)))
		(concatenate 'string output "/")))

;; just for context
(defun cd-up (path)
	(path-concat (butlast (path-split path))))

;; gives full path
(defun path-true (path)
	(let ((pathlist (path-split path))
	      (output ""))
		(if (and (not (null path))
		         (> (length path) 1)
		         (equal (char path 1) #\/))
			(setf output "/")
			(setf output *clls-pwd*))
		(loop for dir in pathlist
			do (if (equal dir "..")
				(setf output (cd-up output))
				(setf output
					(concatenate 'string output "/" dir))))
		output))


;;; --- Internal functions --- ;;;

;; append string list with <str> between them
(defun concat-string-list (str-list &optional (connector " "))
	(let ((output (car str-list)))
		(loop for word in (cdr str-list)
			do (setf output (concatenate 'string output connector word)))
		output))

;; Running binary programs
;(defun run (cmd &key (clls-out :string) (clls-err :string) (clls-wait t))
;	(let (out err ret)
;		(setf (values out err ret)
;			(uiop:run-program cmd :output clls-out
;			                      :error clls-err
;			                      :ignore-error-status t
;			                      :wait clls-wait))
;		(if (not (zerop ret))
;			(format *error-output* "~a~%" err))
;		(setf *clls-last-exit* ret)
;		(if (null out)
;			"" out)))

(defun redirect (foo bar &key (overwrite-append :supersede))
	(with-open-file (str (path-true bar)
	                    :direction :output
	                    :if-exists overwrite-append
	                    :if-does-not-exist :create)
		(format str "~a" foo)))

;; Returns any flag style arguments from an arg list
(defun arg-flags (args)
	(loop for arg in args collect
		(if (equal (char arg 0) #\-)
			arg)))

;;; --- Bash Builtin Functions --- ;;;


;; exit
(defun exit (&optional (code 0))
	(or
		#+sbcl (sb-ext:exit :code code)
		#+ecl  (si:quit)))

;; colon (To be converted from ':')
; Does nothing, absorbs args.
(defun colon (&rest args))

;; cd (Change Directory)
(defun cd (&optional path)
	(let ((try-dir (path-true path)))
		(if (null path)
			(setf try-dir *clls-home*))
		(if (dirp try-dir)
			(change-dir try-dir)
			(format *error-output* "Directory: '~a' does not exist.~%" try-dir))
	*clls-pwd*))

;; pwd (Print Working Directory)
(defun pwd () *clls-pwd*)

;; echo
(defun echo (&rest args)
	(let ((flags (arg-flags args)) outstr
	      newline escape version help strlist
		  (echo-flags '("-e" "-n" "--version" "--help")))

		(loop for arg in args do
			(if (consp (member arg echo-flags :test #'equal))
				(progn
					(if (equal "--version" arg) (setq version t))
					(if (equal "--help" arg) (setq help t))
					(if (equal "-n" arg) (setq newline t))
					(if (equal "-e" arg) (setq escape t)))
				(setq strlist (append strlist (list arg)))))     


		(if version
			(setq outstr "CLLS echo version 1")
			(if help
				(setq outstr "helpful comment")
				(setq outstr (concat-string-list strlist))))

		(if newline
			(format *query-io* "~a~%" outstr)
			(format *query-io* "~a" outstr))))


