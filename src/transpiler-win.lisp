
;;;
;;; Terminal Read Macro & Transpiler (Bash to Lisp)
;;;

(true-path-load "builtins-win.lisp")

;; append string list with <str> between them
(defun concat-string-list (str-list &optional (connector " "))
	(let ((output (car str-list)))
		(loop for word in (cdr str-list)
			do (setf output (concatenate 'string output connector word)))
		output))

;; A function to differ between |, ||, and |&
(defun not-or (str pipe)
	(if (and (not (equal (char str (1- pipe)) #\|))
	         (not (equal (char str (1+ pipe)) #\|)))
		t))

;; Get position of pipelines
(defun pipe-position (str &key start)
	(let ((pipe (position #\| str :start start)))
		(if (numberp pipe)
			(if (not-or str pipe)
				pipe))))

(defun isdelim (str)
	(numberp (position str '(">" ">>" "," ";" "|" "|&" "&"))))

;; White space
(defun isspace (chr)
	(numberp (position chr '(#\Newline #\Space #\Tab
	                         #\Return #\Linefeed))))

(defun space-split (string)
	(loop for start = 0 then (1+ finish)
	      for finish = (position #\Space string :start start)
		collecting (subseq string start finish)
		until (null finish)))

(defun pipe-split (string)
	(loop for start = 0 then (1+ finish)
	      for finish = (pipe-position string :start start)
		collecting (subseq string start finish)
		until (null finish)))
	

(defun compact-cmd (cmd-list &optional (extra ""))
	(let ((single-cmd (concat-string-list
		(loop for tok in cmd-list
			collect tok
			until (isdelim tok)))))
		(concatenate 'string single-cmd extra)))

(defun clls-name (name)
	(let ((builtin name))
		(if (equal name ":")
			"(lambda (&rest args))"
;			(setq builtin "colon"))
		(concatenate 'string "(clls:" builtin))))

(defun run-builtin (cmd-list)
	(handler-case
			(eval (read-from-string
				(concatenate 'string (clls-name (car cmd-list)) " \""
				         (concat-string-list (cdr cmd-list) "\" \"") "\")")))
				(error (e) (format *query-io* "~a~%" e))
				(:no-error (ne) ne)))


(defun safe-run (cmd-list &key (output :string) (extra nil))
	(let ((bin-sym (find-symbol (string-upcase (car cmd-list)) 'clls))
	      (safe-bin (clls-name (car cmd-list))))
		(if (not (null extra))
			(setf cmd-list (append cmd-list (list extra))))
		(if (null bin-sym)
			(clls:run (compact-cmd cmd-list extra) :clls-out output)
			(run-builtin cmd-list))))

;; Transpile
(defun tokenise (line)
	(let* ((pipe-list (pipe-split line)) (prev-out "")
	       (cmd-list (loop for pipe in pipe-list collect (space-split pipe)))
	       (len (length cmd-list)))

		; Run all pipelines with :output as :string
		; except the last one which is run interactively
		(loop for i from 0 below (1- len)
			do (setf prev-out (safe-run (nth i cmd-list) :extra prev-out)))
		(safe-run (nth (1- len) cmd-list) :output :interactive)))



; Control operators:
;        & && ( ) ; ;; | || <newline>
;
; Redirection operators:
;        < > >| << >> <& >& <<- <>


;; Set the dispatch ++ to call this
;; anonymous function on the stream
;; Function: Reads in each line until
;;           End of Line
(defun parser (stream char)
	(declare (ignore char))
	(let ((line (read-line stream nil nil t)))
		(tokenise line)))

(set-macro-character #\! #'parser)
