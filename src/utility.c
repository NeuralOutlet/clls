#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h> // memset

// This is a function for getting direct input from
// input buffer a character at a time without newline needed

// Global structs
struct termios old = {0};
struct sigaction old_action;

// Safety measure for Ctrl + C
void int_handler(int sig)
{
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if (tcsetattr(0, TCSADRAIN, &old) < 0)
		perror ("tcsetattr ~ICANON");
	exit(sig);
}

int getch()
{
	// Set up the ctrl+c trap
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = &int_handler;
	sigaction(SIGINT, &action, &old_action);

	// change the characteristics of the terminal
	// so key input doesn't store in a buffer
	char buf = 0;
	if (tcgetattr(0, &old) < 0)
		perror("tcsetattr()");
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSANOW, &old) < 0)
		perror("tcsetattr ICANON");
	if (read(0, &buf, 1) < 0)
		perror ("read()");
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if (tcsetattr(0, TCSADRAIN, &old) < 0)
		perror ("tcsetattr ~ICANON");

	return (int)(buf);
}


// This is a function for checking if a directory exists
// Takes a string, returns:
//                   1 (true)
//                   0 (false)
//                  -1 (error)
int dirp(char const *d)
{
	int result;
	DIR *dir = opendir(d);
	if (dir)
	{
		closedir(dir);
		result = 1;
	}
	else if(ENOENT == errno)
	{
		fprintf(stderr, "d: %s", d);
		result = 0;
	}
	else
	{
		result = -1;
		fprintf(stderr, "opendir(%s): Failed with errno %d (%#x)",
		        d, errno, errno);
	}

	return result;
}
