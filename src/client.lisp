
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))
(ql:quickload :swank-client)

;; Get swank connection if it already exists
(defvar *myswank* (swank-client:slime-connect "127.0.0.1" 4005))

;; shorthand of evaluating code server-side
(defun scall (code)
	(swank-client:slime-eval code *myswank*))

(defvar *clls-mode* 'lisp)
(defun read-input ()
	(if (equal *clls-mode* 'lisp)
		(format *query-io* "~a> " (scall '(clls:cwd)))
		(format *query-io* "~a$ " (scall '(clls:cwd))))
	(force-output *query-io*)
	(read-line *query-io*))

(defvar in-line "")
(defun terminal-repl ()
	(if (equal *clls-mode* 'shell)
		(setf in-line (concatenate 'string "!" (read-input)))
		(setf in-line (read-input)))
	;; pass a quoted sexp to scall from the user
	(format *query-io* "--> ~a~%" `,(read-from-string in-line))
	(format *query-io* "~a~%" (scall `,(read-from-string in-line)))
	(force-output *query-io*))

;; Function to start swank client repl
(defun start-client (myswank)
	(format t "Connected to ~a~%" myswank)
	(loop until (or (equal in-line "(quit)")
	                (equal in-line "(exit)"))
		do (terminal-repl))
	(or #+ecl (quit) #+sbcl (exit)))

;; Begin either server or client
(if (null *myswank*)
	(format t "No server running")
	(start-client *myswank*))
