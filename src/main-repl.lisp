
(true-path-load "transpiler.lisp")

(defun colour-str (str col)
	(format nil "~C[~am~a~C[0m" #\Esc col str #\Esc))

(defvar *input-length* 0)
(defvar *cursor-x* 0)
(defvar *is-shell* nil)

(defun safe-eval (in-line)
	(if (or (and (equal in-line "!lisp") *is-shell*)
	        (and (equal in-line "(shell)") (not *is-shell*)))
		(progn (setq *is-shell* (not *is-shell*)) t)
		(handler-case
				(eval (read-from-string in-line))
			(error (e) (colour-str (format nil "~a~%" e) 31))
			(:no-error (ne) ne))))

(defun catch-special (str)
	(let ((strlen (length str)))
		(if (and (> strlen 2)
		         (eq (char str (- strlen 3)) #\Esc)
		         (eq (char str (- strlen 2)) #\[))
			(char str (- (length str) 1)))))

(defun apply-special (chr)
	(setq *input-length* (- *input-length* 3))
	(setq *cursor-x* (- *cursor-x* 3))
	(cond
		((and (eq chr #\C) (<= *cursor-x* (1+ *input-length*)))
			(progn
				(setq *cursor-x* (1+ *cursor-x*))
				(format nil "~a" #\C)))
		 ((and (eq chr #\D) (> *cursor-x* 0))
			(progn
				(setq *cursor-x* (1- *cursor-x*))
				(format nil "~a" #\D)))

		(t (format nil "~a~a" #\Backspace #\Backspace))))

(defun catch-input ()
	(let ((key (clls:getch)) (output "") (special-output ""))
		(loop until (or (eq (code-char key) #\Newline)
		                (eq key 20))
			do (progn
				(setq *input-length* (1+ *input-length*))
				(setq *cursor-x* (1+ *cursor-x*))
				(setq output (concatenate 'string output (string (code-char key))))
				(setq special-output (catch-special output))
				(if special-output
					(progn
						(setq output (subseq output 0 (- (length output) 3))) ; remove special garbage
						(format *query-io* "~a" (apply-special special-output)))
					(format *query-io* "~a" (string (code-char key))))
				(force-output *query-io*)
				(setq key (clls:getch))))

		(format t "len: ~a, cur: ~a~%" *input-length* *cursor-x*)
		(setq *input-length* 0)
		(setq *cursor-x* 0)
		(if (eq key 20)
			(progn
				(setf *is-shell* (not *is-shell*)) "")
				output)))

(defun pcname () (machine-instance))
(defun read-input ()
	(if *is-shell*
		(format *query-io* "~a@~a:~a$ " (clls:user) (pcname) (clls:cwd))
		(format *query-io* "~a@~a:~a> " (clls:user) (pcname) (clls:cwd)))
	(force-output *query-io*)
	(if *clls-advanced*
		(catch-input)
		(read-line *query-io*)))

(defvar in-line "")
(defun terminal-repl ()
	(if *is-shell*
		(setf in-line (concatenate 'string "!" (read-input)))
		(setf in-line (read-input)))
	(format *query-io* "~%") ; blank input
	(if (or (and (equal in-line "!") *is-shell*)
	        (equal in-line ""))
		(format *query-io* "~%") ; blank input
		(format *query-io* "~a~%" (safe-eval in-line)))
	(force-output *query-io*))

;; Get commandline arguments
;; for multiple interpretors
(defun get-command-line-args ()
	(or
		#+SBCL (cdr *posix-argv*)
		#+ECL (cdr (si::command-args))
		nil))

(defun main ()
	(if (consp (member "-t" (get-command-line-args) :test #'equal))
		(setq *is-shell* t))
	(format t "~%Common Lisp Linux Shell~%")
	(format t "-----------------------~%")
	(format t "Use the lisp function '(shell)' and the shell function 'lisp' to ")
	(format t "swap between a Bourne Shell and Lisp REPL. ")
	(if *clls-advanced*
		(format t "The hotkey combination <ctrl> + T will also swap between terminal types.~%"))
	(format t "~%Present shell functions: echo, pwd, cd, lisp, exit.~%")
	(format t "Special lisp functions:  defcmd, shell.~%~%")

	(loop until (or (equal in-line "(quit)")
	                (equal in-line "(exit)"))
		do (terminal-repl))
	(or #+ecl (quit) #+sbcl (exit)))
