#!/bin/bash

# Default values:
CL_FLAVOUR="sbcl --script"
BUILD_ADVANCED="nil"

# Get arguments for build
for i in "$@"
do
case $i in
	-e|--ecl)
		CL_FLAVOUR="ecl -q -norc -shell"
		;;
	-s|--sbcl)
		CL_FLAVOUR="sbcl --script"
		;;
	-b|--basic)
		BUILD_ADVANCED=nil
		;;
	-a|--advanced)
		BUILD_ADVANCED=t
		;;
	-c|clean|--clean)
		echo "rm -rf lib/ bin/"
		rm -rf lib/ bin/
		echo "rm src/*.o"
		rm src/*.o
		exit
		;;
	*)
		echo "Argument '${i}' is not recognised."
		;;
esac
done

# Check for quicklisp
if [ ! -d $HOME/quicklisp ]; then

	echo "Making quicklisp directory"
	mkdir $HOME/quicklisp
	cd $HOME/quicklisp

	echo "Downloading quicklisp"
	wget https://beta.quicklisp.org/quicklisp.lisp
	if [ $? -gt 0 ]; then
		echo -e "\n\033[0;31mwget failed.\033[0m\n"
		echo "\ttry running 'sudo apt-get install wget'"
		echo "\tif 'wget --version' shows less than 1.15.\n"
		exit
	fi

	echo "Setting up quicklisp with '$CL_FLAVOUR'"
	cat > temp.lisp <<- LISP_END
	(load "quicklisp.lisp")
	(quicklisp-quickstart:install)
	LISP_END

	$CL_FLAVOUR temp.lisp
	if [ $? -gt 0 ]; then
		echo -e "\n\033[0;31mQuicklisp install failed.\033[0m\n"
		BUILD_ADVANCED="nil"
	fi

	rm temp.lisp
	cd -
fi

# Create library
mkdir -p lib
cc src/utility.c -shared -fPIC -o lib/libcl_utility.so

# If the utility lib built fine then carry on
if [ $? -eq 0 ]; then
	# Create the output folder
	mkdir -p bin

	# flag for the terminal type
	export CLLS_BUILD_ADVANCED=${BUILD_ADVANCED}

	# Call a small lisp script to load everything
	# into the enviroment then save the image
	cat > temp.lisp <<- LISP_END
	(defvar *clls-src-path* (concatenate 'string "$PWD" "/src"))
	(defvar *clls-advanced* ${BUILD_ADVANCED})
	(defun true-path-load (file)
		(load (concatenate 'string *clls-src-path* "/" file)))
	(true-path-load "main-repl.lisp")
	
	(or
		#+sbcl (sb-ext:save-lisp-and-die "$PWD/bin/clls" :toplevel #'main :executable t)
		#+ecl (progn
			(compile-file "${PWD}/src/main-repl.lisp" :system-p t)
			(c:build-program "bin/clls" :lisp-files '("${PWD}/src/main-repl.o")
			                            :prologue-code '(progn
			                                (defvar *clls-src-path* (concatenate 'string "$PWD" "/src"))
			                                (defvar *clls-advanced* ${BUILD_ADVANCED})
			                                (defun true-path-load (file)
			                                (load (concatenate 'string *clls-src-path* "/" file))))
			                            :epilogue-code '(quit (main)))))
	LISP_END

	# Call the desired interpreter
	# with the lisp build script
	$CL_FLAVOUR temp.lisp

	# Exit on success if we are done
	if [ $? -eq 0 ]; then
		echo -e "\n\033[0;32mBuild Complete.\033[0m\n"
	fi
	rm temp.lisp
	exit
fi

# :(
echo -e "\n\033[0;31mBuild Failed.\033[0m\n"

