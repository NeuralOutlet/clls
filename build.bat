echo off

mkdir bin

REM Call a small lisp script to load everything
REM into the enviroment then save the image
(
	echo (defvar *clls-src-path* "\src"^)
	echo:
	echo (defun true-path-load (file^)
	echo 	(load (concatenate 'string *clls-src-path* "\\" file^)^)^)
	echo:
	echo (true-path-load "repl-basic.lisp"^)
	echo:
	echo (main^)
REM	echo (or
REM	echo 	#+sbcl (sb-ext:save-lisp-and-die "%cd%\bin\clls" :toplevel #'main :executable t^)
REM	echo 	#+ecl (progn
REM	echo 		(compile-file "%cd%\src\repl-basic.lisp" :system-p t^)
REM	echo 		(c:build-program "bin\clls" :lisp-files '("%cd%\src\repl-basic.o"^)
REM	echo 		                            :prologue-code '(progn
REM	echo 		                                (defvar *clls-src-path* (concatenate 'string "%cd%" "\src"^)^)
REM	echo 		                                (defun true-path-load (file^)
REM	echo 		                                (load (concatenate 'string *clls-src-path* "\\" file^)^)^)^)
REM	echo 		                            :epilogue-code '(quit (main^)^)^)^)^)
) > temp.lisp

REM Call the desired interpreter
REM with the lisp build script
sbcl --script temp.lisp

REM Exit on success if we are done
REM if errorlevel 0 (
REM	echo ^<ESC^>[92m [92mBuild successful![0m
REM 	rm temp.lisp
REM ) else (
REM	echo ^<ESC^>[91m [91mBuild failed :([0m
REM )

