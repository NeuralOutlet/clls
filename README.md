# Common Lisp Linux Shell #

This is a Lisp/sh dual terminal. Essentially a Lisp REPL with a Bourne Shell interpreter and read macro syntax to emulate a standard Linux terminal.

### Building ###

The build script requires you to have GCC, SBCL or ECL. The options [-a,-b,-e,-s] or [--advanced, --basic, --ecl, --sbcl] can be given to build the basic or slightly more interactive (but buggy) REPL and to use either SBCL or ECL as the core interpreter.

    ./build.sh -a -s  # Advanced SBCL
    ./build.sh --ecl  # Basic ECL

### Running ###

The binary should be located in the bin folder, it will eventually have parameters for various features such as starting in terminal mode and running a script.

    $ ./bin/clls
    Common Lisp Linux Shell
    -----------------------
    Use the lisp function '(shell)' and the shell function 'lisp' to swap between a Bourne Shell and Lisp REPL.
    
    /home/daniel/BitBucket/clls>

### Common Lisp Linux Shell (CLLS)  ###

CLLS is a Bourne Shell interpretor that works from a read macro (!) and GNOME terminal compatible frontend. It consists of:

* Lisp versions of the Bourne builtin functions
* A generic way of running binaries (e.g., ls, grep, etc)
* A transpiler of Bourne-to-CL for control flow (pipe, redirect, if, loop, etc)
* A CLI frontend that can swap between Lisp and Shell modes
* A special function 'defcmd' for adding new Shell commands

It starts up in lisp mode and the read macro can be used in this mode by prefixing '!' to the shell command:

    /home/daniel/BitBucket/clls> !grep -r defcfun ./
    ./src/builtins.lisp:(cffi:defcfun "dirp" :int
    ./src/builtins.lisp:(cffi:defcfun "getch" :int)
    
    /home/daniel/BitBucket/clls> 

All the builtins can be redefined as they are only lisp functions. The transpiler uses a mechanism to check for functions defined in the clls package, if they don't exist it assumes that must be a binary and calls it. This means you can also 'cover up' linux commands by redefining them with '(defcmd)' like this:

    daniel@mypc:/home$ ls
    agamemnon    daniel    illyria
    
    daniel@mypc:/home$ lisp
    daniel@mypc:/home> (defcmd ls () (print "No more ls for you!"))
    LS
    daniel@mypc:/home> (shell)
    daniel@mypc:/home$ ls
    No more ls for you!

In the same fashion an extra file could be loaded with all the defcmds to extend Bourne to bash. You can see above that '(shell)' is the lisp function for swapping to the Bourne shell and 'lisp' is the shell function for swapping to the Lisp REPL. If clls is built with the advanced REPL then you can swap between them using CTRL + T.

### Similar Project by Other People ###

Along with the library [inferior-shell](http://quickdocs.org/inferior-shell/) there are some projects with the same feel:

* [Shelisp](http://dan.corlan.net/shelisp/) - A single-file load method to turn your Lisp REPL into a shell (I use the '!' character because of this).
* [Shelly](https://github.com/fukamachi/shelly)  - A script to run CL commands in the command line but less 'lispy'.
* [Clisp](http://www.cliki.net/CLISP-Shell)   - The well known interpreter can be a usable value for $SHELL with some tweaking.
* [Avesh](https://gitlab.com/ralt/avesh)   - Previously called Clash, an extensable shell written in Lisp.
* [Clesh](https://github.com/Neronus/clesh)   - A fork of Shelisp with added features.